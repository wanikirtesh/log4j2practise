package com.sbn;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

public class NewTestFile {
    private static Logger logger = LogManager.getLogger(NewTestFile.class);
    @Test
    public void mySimpleTest(){
        logger.debug("I am a Logger with log4j 2");
    }
}
