package com.sbn;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DoSomething {
    private static final Logger logger = LogManager.getLogger(DoSomething.class);
    public static void main(String[] args) {
        logger.debug("hello");
        logger.info("hello");
        logger.warn("hello");
        logger.fatal("hello");
        logger.error("hello");
        logger.trace("hello");

    }


}
